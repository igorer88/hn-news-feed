export interface FeedType {
  _id: string;
  title: string;
  story_title: string;
  story_text: string;
  story_url: string;
  story_id: string;
  objectID: string;
  url: string;
  points: string;
  description: string;
  body: string;
  author: string;
  tags: [any];
  deleted: boolean;
  created_at: string;
  updated_at: string;
}
