import { Dispatch } from 'react';

export interface GlobalStateInterface {
  feedList: Array<Object>;
  loading: boolean;
  error: any;
}

export type StateType = {
  state: Object;
};

export type ActionType = {
  type: string;
  payload?: any;
};

export type ContextType = {
  dispatch: Dispatch<ActionType>;
};
