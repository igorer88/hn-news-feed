/* eslint-disable no-unused-vars */
import axios, { AxiosRequestConfig } from 'axios';

export interface APIResponse {
  code?: string | number;
  message: string;
  payload: [any] | object;
}

declare module 'axios' {
  export interface AxiosInstance {
    request<APIResponse = any>(config: AxiosRequestConfig): Promise<APIResponse>;
    get<APIResponse = any>(url: string, config?: AxiosRequestConfig): Promise<APIResponse>;
    delete<APIResponse = any>(url: string, config?: AxiosRequestConfig): Promise<APIResponse>;
    head<APIResponse = any>(url: string, config?: AxiosRequestConfig): Promise<APIResponse>;
    post<APIResponse = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<APIResponse>;
    put<APIResponse = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<APIResponse>;
    patch<APIResponse = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<APIResponse>;
  }
}
