/* eslint-disable no-unused-vars */
/// <reference types="vite/client" />

interface ImportMetaEnv {
  VITE_APP_TITLE: string;
  VITE_APP_NAME: string;
  VITE_API_HOST: string;
  VITE_API_PORT: string;
  VITE_API_PROTOCOL: string;
}
