import '../styles/App.css';

import React from 'react';
import Content from 'src/components/Content';

import Store from '@/store/Store';

function App() {
  return (
    <div className="App">
      <Store>
        <header className="app-header">
          <div className="hero">
            <h1>HN Feed</h1>
            <h2>{'We <3 Hacker News'}</h2>
          </div>
        </header>
        <main className="main-content">
          <Content />
        </main>
      </Store>
    </div>
  );
}

export default App;
