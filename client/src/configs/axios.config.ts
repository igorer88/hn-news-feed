import axios from 'axios';
import { APIResponse } from 'src/types.d/axios';

const apiProtocol = import.meta.env.VITE_API_PROTOCOL || 'http://';
const apiHost = import.meta.env.VITE_API_HOST || 'localhost';
const apiPort = import.meta.env.VITE_API_PORT || 5000;

const baseURL = `${apiProtocol}${apiHost}:${apiPort}/`;

const axiosInstance = axios.create({
  baseURL
});

axiosInstance.interceptors.response.use(
  (response): APIResponse => {
    return response.data;
  },
  error => {
    console.error(error);
    return Promise.reject(error);
  }
);

export default axiosInstance;
