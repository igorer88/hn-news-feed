import React, { createContext, Dispatch, useReducer } from 'react';

import { GlobalStateInterface } from '../types.d/store';
import Reducer from './Reducer';

const initialState: GlobalStateInterface = {
  feedList: [],
  loading: false,
  error: null
};

const Store = ({ children }: { children: React.ReactNode }): React.ReactElement<any, any> => {
  const [state, dispatch] = useReducer(Reducer, initialState);

  return <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>;
};

export const Context = createContext<[GlobalStateInterface, Dispatch<any>]>([initialState, () => {}]);
export default Store;
