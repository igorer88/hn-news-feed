import { ActionType, GlobalStateInterface } from 'src/types.d/store';

const Reducer = (state: GlobalStateInterface, action: ActionType): GlobalStateInterface => {
  switch (action.type) {
    case 'SET_FEEDS':
      return {
        ...state,
        feedList: action.payload
      };
    case 'REMOVE_FEED':
      return {
        ...state,
        feedList: state.feedList.filter((feed: any) => feed._id !== action.payload)
      };
    case 'SET_LOADING':
      return {
        ...state,
        loading: action.payload
      };
    case 'SET_ERROR':
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
};

export default Reducer;
