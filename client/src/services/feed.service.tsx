import { APIResponse } from 'src/types.d/axios';

import axios from '@/configs/axios.config';

export const getFeeds = async (): Promise<any> => {
  try {
    const feeds = await axios.get<APIResponse>('feeds');
    return Promise.resolve(feeds.payload);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteFeed = async (feedId: string): Promise<any> => {
  try {
    const deletedFeed = await axios.delete(`feeds/${feedId}`);
    return Promise.resolve(deletedFeed.payload);
  } catch (error) {
    return Promise.reject(error);
  }
};
