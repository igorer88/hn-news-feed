import '@/styles/Content.css';

import React from 'react';

import FeedsContainer from './Feeds/FeedContainer';

interface ContentProps {}

const Content = (): React.ReactElement<ContentProps> => {
  return (
    <section className="container content">
      <FeedsContainer />
    </section>
  );
};

export default Content;
