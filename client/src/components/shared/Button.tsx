/* eslint-disable no-unused-vars */
import React from 'react';

interface ButtonProps {
  children?: React.ReactChild;
  title?: string;
  className: string;
  clickHandler: (ev: any) => Promise<void>;
}

const Button: React.FC<ButtonProps> = props => {
  const { children, title, clickHandler, className } = props;

  const onKeyPressHandler = () => {};

  return (
    <div
      role="button"
      tabIndex={0}
      title={title}
      className={className}
      onKeyPress={onKeyPressHandler}
      onClick={clickHandler}
    >
      {children}
    </div>
  );
};

export default Button;
