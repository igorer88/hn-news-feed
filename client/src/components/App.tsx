import '../styles/App.css';

import React from 'react';

import Store from '@/store/Store';

import Content from './Content';

function App() {
  return (
    <div className="App">
      <Store>
        <header className="app-header">
          <div className="hero">
            <h1>HN Feed</h1>
            <h2>{'We <3 Hacker News'}</h2>
          </div>
        </header>
        <main className="main-content">
          <Content />
        </main>
      </Store>
    </div>
  );
}

export default App;
