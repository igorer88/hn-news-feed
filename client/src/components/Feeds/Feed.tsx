import '@/styles/Feed.css';

import * as moment from 'moment';
import React, { useContext } from 'react';
import { FaRegTrashAlt } from 'react-icons/fa';
import { FeedType } from 'src/types.d/feed';

import Button from '@/components/shared/Button';
import { deleteFeed, getFeeds } from '@/services/feed.service';
import { Context } from '@/store/Store';

const today = moment().endOf('day');

const formatDate = (date: string): String => {
  const feedDate = moment(date);
  const diff = today.diff(feedDate, 'hours', true);

  if (diff < 0) {
    return 'N/A'; // future datatime
  } else if (diff < 24) {
    return feedDate.format('hh:mm a'); // today
  } else if (diff < 48) {
    return 'Yesterday';
  }

  return feedDate.format('MMM d');
};

const displayText = (feed: FeedType) => {
  return feed.story_title ? feed.story_title : feed.title;
};

const handleLink = (feed: FeedType) => {
  return feed.story_url ? feed.story_url : feed.url;
};

interface FeedProps {
  feed: FeedType;
}

const Feed = (props: FeedProps): React.ReactElement<FeedProps> => {
  const { feed } = props;
  // eslint-disable-next-line no-unused-vars
  const [state, dispatch] = useContext(Context);

  const handleDelete = async (ev: Event): Promise<void> => {
    ev.preventDefault();
    const result = window.confirm(
      `Are you sure you want to delete the feed: ${displayText(feed)} with id: ${feed._id}?`
    );
    if (result) {
      try {
        await deleteFeed(feed._id);
        dispatch({ type: 'SET_LOADING', payload: true });
        const list = await getFeeds();
        dispatch({ type: 'SET_FEEDS', payload: list });
        window.alert('Feed has been deleted.');
      } catch (error: any) {
        dispatch({ type: 'SET_ERROR', payload: error });
        window.alert(error.message);
      } finally {
        dispatch({ type: 'SET_LOADING', payload: false });
      }
    }
  };

  return (
    <a className="feed__item" href={handleLink(feed)} target="_blank" rel="noreferrer">
      <div className="feed__item--info">
        <div className="feed__item--title">{displayText(feed)}</div>
        <div className="feed__item--author">{`- ${feed.author} -`}</div>
      </div>
      <div className="feed__item--createdAt">
        <span>{formatDate(feed.created_at)}</span>
      </div>
      <Button className="feed__item--action" title="Delete feed" clickHandler={handleDelete}>
        <FaRegTrashAlt />
      </Button>
    </a>
  );
};

export default Feed;
