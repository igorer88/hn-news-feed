import React, { useContext, useEffect } from 'react';
import { FeedType } from 'src/types.d/feed';

import { getFeeds } from '@/services/feed.service';
import { Context } from '@/store/Store';

import Feed from './Feed';

const hasTitle = (feed: FeedType): boolean => {
  if (feed.story_title && feed.story_title !== '') {
    return true;
  }
  if (feed.title && feed.title !== '') {
    return true;
  }
  return false;
};

interface FeedsContainerProps {}

const FeedsContainer = (): React.ReactElement<FeedsContainerProps> => {
  const [state, dispatch] = useContext(Context);

  const fetchFeeds = async (): Promise<void> => {
    try {
      dispatch({ type: 'SET_LOADING', payload: true });
      const list = await getFeeds();
      dispatch({ type: 'SET_FEEDS', payload: list });
    } catch (error) {
      dispatch({ type: 'SET_ERROR', payload: error });
    } finally {
      dispatch({ type: 'SET_LOADING', payload: false });
    }
  };

  useEffect(() => {
    fetchFeeds();
  }, []);

  return (
    <>
      {state.loading && <p>Loading...</p>}
      {!state.loading && state.error && (
        <>
          <p>
            Something went wrong: <b>{state.error.message}</b>
          </p>
          <p>
            check your connection and <em>refresh the page</em>.
          </p>
        </>
      )}
      {!state.loading &&
        state.feedList &&
        state.feedList.map((feed: any) => {
          return hasTitle(feed) && !feed.deleted && <Feed key={feed._id} feed={feed}></Feed>;
        })}
    </>
  );
};

export default FeedsContainer;
