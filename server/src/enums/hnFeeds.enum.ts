export enum HNFeeds {
  API = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
}

export enum ErrorCodes {
  DB_UPDATED = 'DB_UPDATED',
}

export enum ErrorMessages {
  COULD_NOT_CONNECT_TO_API = 'Could not connect to HN API',
  FEEDS_ALREADY_UPDATED = 'Feeds up to date',
}
