import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HNFeedService } from './feed/hn-feed.service';
const port = process.env.API_PORT || 5000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: '*',
  });
  await app.listen(port);
  const hmFeedService = app.get(HNFeedService);
  hmFeedService.getHNFeeds();
}

bootstrap();
