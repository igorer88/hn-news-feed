import {
  Controller,
  Get,
  Post,
  Delete,
  Res,
  Param,
  Body,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import { FeedService } from './feed.service';
import { CreateFeedDTO } from './dto/create-feed.dto';
import { ValidateObjectId } from './shared/pipes/validate-object-id.pipes';

@Controller('feeds')
export class FeedController {
  constructor(private feedService: FeedService) {}

  // Fetch all feeds
  @Get('/')
  async getFeeds(@Res() res) {
    const feeds = await this.feedService.getFeeds();
    return res.status(HttpStatus.OK).json({
      message: 'Feeds has been successfully fetched',
      payload: feeds,
    });
  }

  // Create a feed
  @Post('/')
  async addFeed(@Res() res, @Body() createFeedDTO: CreateFeedDTO) {
    const newFeed = await this.feedService.addFeed(createFeedDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Feed has been submitted successfully!',
      payload: newFeed,
    });
  }

  // Fetch a particular feed using ID
  @Get('/:feedID')
  async getFeed(@Res() res, @Param('feedID', new ValidateObjectId()) feedID) {
    const feed = await this.feedService.getFeed(feedID);
    if (!feed) {
      throw new NotFoundException('Feed does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Feed has been successfully fetched',
      payload: feed,
    });
  }

  // Delete a particular feed using ID
  @Delete('/:feedID')
  async deleteFeed(
    @Res() res,
    @Param('feedID', new ValidateObjectId()) feedID,
  ) {
    const deletedFeed = await this.feedService.softDeleteFeed(feedID);
    if (!deletedFeed) {
      throw new NotFoundException('Feed does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Feed has been successfully deleted',
      payload: deletedFeed,
    });
  }
}
