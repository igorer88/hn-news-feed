import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Feed } from './interfaces/feed.interface';
import { CreateFeedDTO } from './dto/create-feed.dto';

@Injectable()
export class FeedService {
  constructor(@InjectModel('Feed') private readonly feedModel: Model<Feed>) {}

  async getFeeds(): Promise<Feed[]> {
    const feeds = await this.feedModel.find().sort({ created_at: -1 }).exec();
    return feeds;
  }

  async addFeed(createFeedDTO: CreateFeedDTO): Promise<Feed> {
    const newFeed = await new this.feedModel(createFeedDTO);
    return newFeed.save();
  }

  async getFeed(feedID: string): Promise<Feed> {
    const feed = await this.feedModel.findById(feedID).exec();
    return feed;
  }

  async softDeleteFeed(feedID: string, restore?: boolean): Promise<any> {
    const value = restore ? false : true;
    const deletedFeed = await this.feedModel.findByIdAndUpdate(feedID, {
      deleted: value,
    });
    return deletedFeed;
  }
}
