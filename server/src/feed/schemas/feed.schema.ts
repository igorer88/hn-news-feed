import * as mongoose from 'mongoose';

export const FeedSchema = new mongoose.Schema(
  {
    title: String,
    story_title: String,
    story_text: String,
    story_url: String,
    story_id: String,
    objectID: String,
    url: String,
    points: String,
    description: String,
    body: String,
    author: String,
    tags: [Object],
    deleted: { type: Boolean, default: false },
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } },
);
