import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { FeedSchema } from './schemas/feed.schema';
import { HNFeedService } from './hn-feed.service';
import { FeedService } from './feed.service';
import { FeedController } from './feed.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Feed', schema: FeedSchema }]),
    ScheduleModule.forRoot(),
    HttpModule,
  ],
  providers: [FeedService, HNFeedService],
  controllers: [FeedController],
})
export class FeedModule {}
