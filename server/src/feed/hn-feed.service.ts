import { map, Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Feed } from './interfaces/feed.interface';
import { HNFeeds, ErrorCodes, ErrorMessages } from '../enums/hnFeeds.enum';

@Injectable()
export class HNFeedService {
  constructor(
    @InjectModel('Feed') private readonly feedModel: Model<Feed>,
    private readonly httpService: HttpService,
  ) {}
  private readonly logger = new Logger(HNFeedService.name);

  @Cron(CronExpression.EVERY_HOUR)
  async getHNFeeds(): Promise<Observable<AxiosResponse<Feed[]>>> {
    try {
      this.logger.log('Getting Hacker News feeds');
      const feeds = await this.httpService
        .get(HNFeeds.API)
        .pipe(map((response) => response.data))
        .toPromise();

      await this.insertHNFeeds(feeds.hits);
      this.logger.log('Finished loading HN Feeds into DB.');

      return feeds;
    } catch (error) {
      if (error.code === ErrorCodes.DB_UPDATED) {
        this.logger.log(error.message);
      } else {
        this.logger.warn(ErrorMessages.COULD_NOT_CONNECT_TO_API, error);
      }
    }
  }

  async insertHNFeeds(feeds: Array<any>): Promise<any> {
    try {
      const currentFeeds = await this.feedModel.find();

      this.logger.debug('Checking HN feeds in DB');
      if (currentFeeds.length < feeds.length) {
        this.logger.debug('Loading HN feeds into the DB');
        return await this.feedModel.insertMany(feeds);
      } else {
        return Promise.reject({
          code: ErrorCodes.DB_UPDATED,
          message: ErrorMessages.FEEDS_ALREADY_UPDATED,
        });
      }
    } catch (error) {
      this.logger.error('Could not save HN Feeds');
    }
  }
}
