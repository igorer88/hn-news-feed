import { Document } from 'mongoose';

export interface Feed extends Document {
  readonly title: string;
  readonly story_title: string;
  readonly story_text: string;
  readonly story_url: string;
  readonly story_id: string;
  readonly objectID: string;
  readonly url: string;
  readonly points: string;
  readonly description: string;
  readonly body: string;
  readonly author: string;
  readonly tags: [any];
  readonly deleted: boolean;
  readonly created_at: string;
  readonly updated_at: string;
}
