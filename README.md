## Description

Hacker News feeds app built with [MongoDB](https://www.mongodb.com), [Nest](https://github.com/nestjs/nest) framework and [React](https://reactjs.org)

## To run the dev server you need:

- Docker engine & Docker-compose
- Copy the .env.example file and rename it as .env

## DB Management

The server handles enviroment variables as folllows:

    - DB_USER
    - DB_PASSWORD
    - DB_NAME
    - DB_HOST    # Set to docker service
    - DB_PORT
    - API_PORT

> The DB is populated at server start and it automatically runs an update (if there is any) every hour

## Development

You could add `-d` at the end to run it as a daemon

```bash
$ docker-compose up
```

## Alternative

If you have a local instance of MongoDB running in your machine, you could open two separate terminals; one inside the server folder and another one inside client folder and run in each one:

> To install dependencies in each component

```bash
$ yarn install
```

> To start the dev server

```bash
server/ $ yarn start:dev
```

> To start the dev client

```bash
client/ $ yarn run dev
```

> Note: But docker already does that for us ;)

## Stay in touch

- Author - [Igor Escalona](mailto:igorer88@gmail.com)

## License

HN Feeds is [MIT licensed](LICENSE).
